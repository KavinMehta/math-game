var score;
var playing=false;
var timeremaining;
var countdown;
var correctAns;

//Helper
function setText(id, text){
    document.getElementById(id).innerHTML=text;
}

function show(id){
    document.getElementById(id).style.display='block';
}
function hide(id){
    document.getElementById(id).style.display='none';
}

document.getElementById("startreset").onclick=function(){
    if(playing==true){
        //game is on and you tried to reset it.
        playing=false;
        window.location.reload();
    }else{
        //game is off and u wanna start it.
        playing=true;
        
        score=0;
        setText("scoreValue",score);
        
        show("timeremaining");
        timeremaining=10;
        setText("timeremaining",timeremaining);
        
        this.innerHTML="Reset Game";
        hide("gameover");
        startCountDown();
        generateQA();
    }
}

function startCountDown(){
    countdown=setInterval(function(){
        timeremaining--;
        setText("timeremaining",timeremaining);
        if(timeremaining<=0){
            stopCountDown();
            show("gameover");
            playing=false;
            setText("startreset","Start Game!");
            hide("timeremaining");
            setText("scoreValue","");
            setText("gameover","<p>Game Over!</p><p>Your Score is " + score + "</p>");
        }
    },1000);
}

function stopCountDown(){
    clearInterval(countdown);
}

function generateQA(){
    
    var x=(1+Math.round(Math.random()*9));
    var y=(1+Math.round(Math.random()*9));
    correctAns = x*y;
    setText("question",x+"x"+y);
    
    var correctPosition = (1+Math.round(Math.random()*3));
    setText("box"+correctPosition,correctAns);
    
    var answers = [correctAns];
    for(i=1;i<5;i++){
        var wrongAns;
        if(i!=correctPosition){
            
            do{
                wrongAns=(1+Math.round(Math.random()*9))*(1+Math.round(Math.random()*9))
            }while(answers.indexOf(wrongAns)>-1);
            
            answers.push(wrongAns);
            setText("box"+i,wrongAns);
        }
    }
}

for(i=1;i<5;i++){
    document.getElementById("box"+i).onclick=function(){
        if(playing){
            if(correctAns == this.innerHTML){
                score++;
                setText("scoreValue",score);
                show("correct");
                hide("wrong");
                setTimeout(function(){
                    hide("correct");
                },1000);
                
                generateQA();
            }else{
                show("wrong");
                hide("correct");
                setTimeout(function(){
                    hide("wrong");
                },1000);
            }
        }
    }
}